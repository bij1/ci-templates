# CI templates

## Markdown

Generates documents styled with `style.css` + `metadata.txt` from a [markdown][https://markdownguide.org/] file.
For a sample project using this template see [`huishoudelijk-reglement`](https://gitlab.com/bij1/docs/huishoudelijk-reglement/)

### Usage

create a `.gitlab-ci.yml` file in your repo with the following content:

```
include:
  - project: 'bij1/ci-templates'  
    ref: 'main'
    file: 'markdown.yml'

variables:
  PROJECT_ID  : ${CI_PROJECT_ID}
  PROJECT_NAME: ${CI_PROJECT_NAME}
  PROJECT_SLUG: ${CI_PROJECT_PATH_SLUG}
  MARKDOWN_FILE: README.md
```
